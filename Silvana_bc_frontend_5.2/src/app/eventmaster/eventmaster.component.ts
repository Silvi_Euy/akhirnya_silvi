import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-eventmaster',
  templateUrl: './eventmaster.component.html',
  styleUrls: ['./eventmaster.component.css']
})
export class EventmasterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input("event") eventList : Object[];

  newNamaEvent : string = "";
  newTanggal : string = "";
  newJam : string = "";
  newHargaTiket : string = "";
  newLokasi : string = "";
  
  

  registerevent() {

    var id = 1;
    if (this.eventList.length > 0) {
      id = this.eventList[this.eventList.length - 1]["id"] + 1;
    }

    this.eventList.push({"id":id, "namaevent":this.newNamaEvent, "waktu":this.newWaktu,"jam":this.newJam, "hargatiket":this.newHargaTiket, "lokasi":this.newLokasi});

    this.newNamaEvent = "";
    this.newTanggal = "";
    this.newJam = "";
    this.newHargaTiket = "";
    this.newLokasi = "";
    
    
  }


}
