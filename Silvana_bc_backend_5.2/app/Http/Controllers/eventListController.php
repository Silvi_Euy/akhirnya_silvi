<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\eventList;

class eventListController extends Controller
{
    function getData(){

        $userList = UserList::get();

        return response()->json($userList, 200);
    }
}
