<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvenMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('even_masters', function (Blueprint $table) {
            $table->increments('id');

            $table->string('NamaEvent');
            $table->string('Lokasi');
            $table->date('Tanggal');
            $table->string('Jam');
            $table->decimal('HargaTiket');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('even_masters');
    }
}
