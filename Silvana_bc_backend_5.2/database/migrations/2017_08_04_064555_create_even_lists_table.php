<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvenListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('even_lists', function (Blueprint $table) {
            $table->increments('id');

            $table->string('EventA');
            $table->string('EventB');
            $table->string('EventC');
            $table->string('EventD');
            $table->string('EventE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('even_lists');
    }
}
